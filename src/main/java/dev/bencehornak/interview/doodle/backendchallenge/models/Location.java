package dev.bencehornak.interview.doodle.backendchallenge.models;

public class Location {
    public String name;
    public String address;
    public String countryCode;
    public String locationId;
}
