package dev.bencehornak.interview.doodle.backendchallenge.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("polls")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Poll {
    @Id
    public String id;
    public String adminKey;
    public long latestChange;
    public long initiated;
    public int participantsCount;
    public int inviteesCount;
    public String type;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    public int rowConstraint;
    public String preferencesType;
    public String state;
    public String locale;
    public String title;
    public Initiator initiator;
    public Option[] options;
    public String optionsHash;
    public Participant[] participants;
    public Invitee[] invitees;
    public String device;
    public String levels;
    public Location location;
    public String description;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    public boolean multiDay;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    public int columnConstraint;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    public boolean hidden;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    public boolean dateText;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    public boolean timeZone;
}
