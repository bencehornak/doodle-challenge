package dev.bencehornak.interview.doodle.backendchallenge.controllers;

import dev.bencehornak.interview.doodle.backendchallenge.models.Poll;
import dev.bencehornak.interview.doodle.backendchallenge.repositories.PollRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.regex.Pattern;

@RestController
@RequestMapping("/api/v1/users/current")
public class CurrentUserController {

    @Autowired
    private PollRepository pollRepository;

    @GetMapping("/polls")
    public ResponseEntity<Page<Poll>> polls(Pageable pageable,
                                            @RequestParam(required=false, defaultValue = "") String title,
                                            @RequestParam(required=false, defaultValue = "0") long initiatedAfter) {
        return ResponseEntity.ok(
                pollRepository.findByTitleLikeAndInitiatedGreaterThan(
                        pageable,
                        Pattern.quote(title),
                        initiatedAfter));
    }

}
