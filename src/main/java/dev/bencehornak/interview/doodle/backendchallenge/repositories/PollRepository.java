package dev.bencehornak.interview.doodle.backendchallenge.repositories;

import dev.bencehornak.interview.doodle.backendchallenge.models.Poll;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface PollRepository extends MongoRepository<Poll, String>{

    @Query("{ title: { $regex: ?0 }, initiated: {$gt: ?1 } }")
    Page<Poll> findByTitleLikeAndInitiatedGreaterThan(Pageable pageable, String title, long initiated);
}
