package dev.bencehornak.interview.doodle.backendchallenge.models;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Initiator {
    public String name;
    public String email;
    public boolean notify;
    public String timeZone;
}
