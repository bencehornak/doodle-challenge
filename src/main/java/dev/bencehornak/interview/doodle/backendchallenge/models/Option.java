package dev.bencehornak.interview.doodle.backendchallenge.models;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class Option {
    public boolean allday;
    public boolean available;
    public long date;
    public long dateTime;
    public long start;
    public long startDate;
    public long startDateTime;
    public long end;
    public long endDate;
    public long endDateTime;
    public String text;
}
