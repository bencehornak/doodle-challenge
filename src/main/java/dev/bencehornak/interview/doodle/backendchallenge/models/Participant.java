package dev.bencehornak.interview.doodle.backendchallenge.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

public class Participant {
    public long id;
    public String name;
    public int[] preferences;
}
