package dev.bencehornak.interview.doodle.backendchallenge;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.LongNode;
import dev.bencehornak.interview.doodle.backendchallenge.models.Poll;
import dev.bencehornak.interview.doodle.backendchallenge.repositories.PollRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.init.ResourceReader;

import java.io.IOException;
import java.util.Comparator;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class PollTests {

    private static final Logger logger = LoggerFactory.getLogger(PollTests.class);

    @Value("classpath:polls.json")
    private Resource pollsJsonResource;

    @Autowired
    private PollRepository pollRepository;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void contextLoads() {
        assertThat(pollRepository).isNotNull();
    }

    @Test
    void databaseContainsRecords() {
        assertThat(pollRepository.findAll(PageRequest.of(0, 1))).isNotEmpty();
    }

    @Test
    void pollsJsonCompatible() throws IOException {
        // JSON tree representation of polls.json
        JsonNode original = objectMapper.readTree(pollsJsonResource.getInputStream());

        // Represent it using our classes
        Poll[] imported = objectMapper.treeToValue(original, Poll[].class);

        // Export it as a JSON String
        String importedExportedString = objectMapper.writeValueAsString(imported);

        // Parse the JSON String
        // The reason, it is necessary is that Jackson parses some values as int instead of long from the JSON input
        // and because LongNode(x) != IntNode(x), they appear to be unequal
        // https://github.com/FasterXML/jackson-databind/issues/1758
        JsonNode importedExported = objectMapper.readTree(importedExportedString);

        // And compare with the original one
        assertThat(importedExported).isEqualTo(original);
    }

}
