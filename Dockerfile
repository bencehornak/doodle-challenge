# Multi stage Dockerfile

###################################
# First stage: build gradle project
###################################
FROM openjdk:14-jdk-alpine as build

WORKDIR /app

# Add gradle files
COPY build.gradle settings.gradle gradlew ./
COPY gradle gradle

# Download (and cache) dependencies and gradle distribution
RUN ./gradlew build --no-daemon || true

# Add project source code
COPY src src

# Build project
RUN ./gradlew build -x test --no-daemon

###########################################
# Second stage: small size production build
###########################################
FROM openjdk:14-alpine as production

# Run the Spring boot application
ENTRYPOINT ["java", "-jar", "app/app.jar"]

# Add the compiled application
COPY --from=build /app/build/libs/*.jar /app/app.jar
