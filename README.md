# Doodle challenge

## Setup
The project is compiled and run using Docker, so the following docker-compose
commands launches it with zero configuration:
```sh
docker-compose up
```

For development gradle tasks can be used for debugging and testing.

## Architecture
### docker-compose
My goal was to be completely platform agnostic, and not to rely on any other
softwares of the host system than docker-compose. This is why I decided to
invoke the building steps in the [Dockerfile](Dockerfile).

I optimized the Dockerfile for fast rebuilds and slim production image.

### Database
I decided to use MongoDB for the database backend, because I could use the
provided dataset without conversion.

The dataset is embedded to a bootstrap script
([10-insert-polls.js](initdb/10-insert-polls.js)), which is mounted into the
`mongo` container's `/docker-entrypoint-initdb.d` directory.

### Restful backend
The backend is implemented in Java with Spring.

The following endpoints are defined:

#### List all polls of current user
```
GET /api/v1/users/current/polls
```
**GET Parameters:**
- `size` (optional, nonnegative integer): The number of records returned.
- `page` (optional, nonnegative integer): The index of the page returned.
- `title` (optional, nonempty string): Filter results based on the name of the poll.
- `initiatedAfter` (optional, unix timestamp): Return polls only after a specific time

**Returns:** the list of polls plus metadata about the pagination

**Notes:** This endpoint fulfills all functional requirements:
1. *List all polls created by a user*
2. *Search polls by its title*
3. *List all polls created after a certain date*

**Example:**
```sh
curl --location --request GET 'localhost:8080/api/v1/users/current/polls?title=sup&initiatedAfter=1485521569055&size=1&page=0'
```

#### Tests
I wrote only a small number of tests due to the time limit. They must be run
with the provided test database. Tests check whether the repository bean can be
loaded, if a record can be fetched from the database and if the object
representation is compatible with the shape of `polls.json`.

## About the development
### Creating a 'compatible' object representation
I loaded the provided dataset to MongoDB first, so that I could analyze the
shape of the dataset using MongoDB tools (such as MongoDB Compass).

I spent maybe too much time with making my model representation perfectly
compatible with `polls.json`. I wrote a test, that deserializes polls.json using
Jackson's `ObjectMapper` then serializes it back to JSON and see if they are
semantically equal. It turned out, that I missed some fields in the first round
and additionally had to specify, how to serialize null/default values. I used an
online tool (http://www.jsondiff.com/) to understand the differences between the
input and serialized object representation.

If I could start from over, I analyzed the input differently, using some online
JSON schema tool (such as https://jsonschema.net/), because it would be less
error-prone.

### Jackson JSON comparison issue
I encountered an issue when using JSON comparison. Jackson recognized identical
JSON trees as different ones sometimes. I investigated this issue and found the
reason for this.

The context of the problem is that numerical values are deserialized from string
JSON to either an `IntNode` or `LongNode` based on their size. However, when
serializing a `long` field, it always becomes a `LongNode`. So the problem is,
when comparing a `LongNode` with an `IntNode` containing the same value, they
[turn out to be
different](https://github.com/FasterXML/jackson-databind/issues/1758).

I used a workaround here, so before the comparison, [I added an extra
serialization + deserialization
step](src/test/java/dev/bencehornak/interview/doodle/backendchallenge/PollTests.java#L58),
which fixed the issue.

### Spring MongoDB regular expression issue
One other obstacle that required time for debugging was related to Spring's
MongoDB query creation. I wanted to construct a regexp filter that matches to
the values which contain the search term.

I used Spring's automatic implementation features, which can guess how
interfaces should be implemented. For example, given an interface which extends
`MongoRepository`, Spring can automatically create the methods [based on their
name and
signature](https://docs.spring.io/spring-data/data-document/docs/current/reference/html/#mongodb.repositories.queries).

Let's consider the following name:

```java
findByTitleLikeAndInitiatedGreaterThan(String title, long initiated)
```

Based on the docs, it translates to the following query:
```js
{
  "title": titleAsRegex,
  "initiated": { "$gt": intiated }
}
```
This functionality seems to be working as expected, as it produces the following
query for `findByTitleLikeAndInitiatedGreaterThan("superhéros", 0)`:
```json
{
  "title": {
    "$regularExpression": {
      "pattern": "superhéros",
      "options": ""
    }
  },
  "initiated": {
    "$gt": 0
  }
}
```

**Note:** I used the following config parameter to print queries:
`logging.level.org.springframework.data.mongodb.core.MongoTemplate=DEBUG`

It turned out, however, that it applies some transformation to the regexp
parameter, which makes it unsuitable for my intended usage. For example
`findByTitleLikeAndInitiatedGreaterThan("\Qsuperhéros\E", 0)` is converted
to the following:
```json
{
  "title": {
    "$regularExpression": {
      "pattern": "\\Q\\Qsuperhéros\\E\\\\E\\Q\\E",
      "options": ""
    }
  },
  "initiated": {
    "$gt": 0
  }
}
```
which clearly doesn't match to `"Qui sont les superhéros Marvel les plus
oufs?"`, as the search term is double escaped.

**Note:** `\Q`...`\E` is a quote in regexp

Moreover, the escaping is not done properly, as
`findByTitleLikeAndInitiatedGreaterThan("*", 0)` results in this query (!):
```json
{
  "title": {
    "$regularExpression": {
      "pattern": ".*",
      "options": ""
    }
  },
  "initiated": {
    "$gt": 0
  }
}
```

To overcome this, I provided a query skeleton instead of the automatic
implementation:

```java
@Query("{ title: { $regex: ?0 }, initiated: {$gt: ?1 } }")
Page<Poll> findByTitleLikeAndInitiatedGreaterThan(Pageable pageable, String title, long initiated);
```

which works as expected:

```json
{
  "title": {
    "$regex": "\\Qsuperhéros\\E"
  },
  "initiated": {
    "$gt": 0
  }
}
```

## If I had more time
- I would implement authentication and authorization.
- I would filter to the current users records.
- I would write unit tests for the endpoint that test each parameters,
  including their validation
- I would write unit tests that check the reponses' status code and payload.
- I would check if the regexp is escaped properly, not allowing [ReDoS](https://owasp.org/www-community/attacks/Regular_expression_Denial_of_Service_-_ReDoS).
- I would test if improper calls [are handled gracefully](https://owasp.org/www-community/Improper_Error_Handling), not leaking too much
  information about the system.
- I would write integration tests to see if data is read properly from database.
- I would write CI/CD scripts, so that Docker image was built in the cloud.
- I would have chosen a git workflow, so that each feature could have had their
  seperate branch.
